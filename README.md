---
layout: software
title:  'Tools About Anycast Agility Against DDoS in Tangled Testbed'
withreleasenotes: true
sw_path: software/anygility-tangled.tar.gz
---


# anygility - anycast agility tools

This page contains tools to defend against Denial of Service (DoS) attacks using anycast agility. These tools are part of the paper **Anycast Agility: Network Playbooks to Fight DDoS**. As described in the paper, we've used two testbeds to deploy our experiments. Each testbed has it own setup and tools, therefore we've organized the tools as following:
- Tangled Testbed: tools and dataset (*this page*)
- Peering Testbed: [tools and dataset](https://ant.isi.edu/software/anygility/index.html)

The main idea behind *anygility* is previously quantify the anycast catchment distribution in different BGP routing settings (BGP-TE), and to use this knowledge to reconfigure the anycast network when under DDoS. The anycast routing reconfiguration aims to better redistribute the DDoS overload over sites making the anycast service more resilient. To do so, we create *anycast playbooks* to be applied in case of attacks. Basically, each *playbook* contains the routing settings, and the load distribution between sites when such routing policy is applied.


# Tools and Setup Configurations in Tangled

Here we provide the tools we used to perform anygility experiments on the [TANGLED: ANYCAST TESTBED](https://anycast-testbed.nl/). 

<ul>
<li><code class="language-plaintext highlighter-rouge"> measurement scripts </code>: a series of scripts to implement the measurement process, collect catchment statistics and playbook creation.
<li><code class="language-plaintext highlighter-rouge"> tangler-cli </code>: tangler-cli (Tangled Routing tool) is used to control BGP routing in an anycast networks. This tool uses ExaBGP software to inject routes on each anycast site.
<li><code class="language-plaintext highlighter-rouge"> vp-cli.py </code>: process Verfploeter raw files to generate the statistics used to build the anycast playbook.
<li><code class="language-plaintext highlighter-rouge"> bgp-tuner </code>: graphical interface to manage anycast networks using catchment statistics.
<li><code class="language-plaintext highlighter-rouge"> make-playbook </code>: use statistics and routing information to create the playbook file.
<li><code class="language-plaintext highlighter-rouge"> run-playbook </code>: run a selected playbook policy applying one BGP routing policy in anycast sites.
</ul>

## Summary

A set of *measurement scripts* to generate the Playbook on different routing setups (baseline, positive and negative prepends, bgp communities and bgp poisoning). These scripts make use of *Tangler-cli* (to control anycast routing on Tangled testbed); *Verfploeter* software to measure the catchment distribution; and *vp-cli* to analyze the verfploeter data and generate the site load statistics for each routing setup. We provide an prototyped graphical interface (*bgp-tuner*) to visualize the Playbooks available and help the administrator to choosing which Playbook entry to apply. We also provide *run-playbook* tool to implement the chosen Playbook entry on Tangled network.



# COMPONENTS

## Measurement Scripts

Measurements are the core for the measurement process. They (1) established a *connection* to each anycast node; (2) call *tangler-cli* to recursively setup BGP routing and store the configuration for all sites;  (3) call *Verfploeter* to measure catchment; and finally (3) call *vp-cli* to account catchment stats for one routing policy. We used the following scripts:

<ul>
<li><code class="language-plaintext highlighter-rouge"> 00-functions.sh </code>: basic functions used for all scripts
<li><code class="language-plaintext highlighter-rouge"> 00-functions.sh </code>: the config file for paths and other parameters used on all scripts.
<li><code class="language-plaintext highlighter-rouge"> 01-baseline.sh </code>: script to create a baseline for all anycast sites
<li><code class="language-plaintext highlighter-rouge"> 02-positive_prepend.sh </code>: BGP path prepend measurement
<li><code class="language-plaintext highlighter-rouge"> 03-negative_prepend.sh </code>: Negative prepend measurement
<li><code class="language-plaintext highlighter-rouge"> 04-communities.sh </code>: BGP community experiment
<li><code class="language-plaintext highlighter-rouge"> 05-poison.sh </code>:  BGP Poisoning measurement
</ul>

After running one of these scripts, you should obtain five files:

- ``*.csv.gz``: The raw file produced by Verfploeter (v1.0.42). This file contains the ICMP answers in CSV format.
- ``*.meta``: Meta information about how Verfploeter data was used to produce *.routing* and *.stats* files with *vp-cli*.
- ``*.meta-convert``: Meta information to *vp-cli* normalize measurement considering network weight and geolocation.
- ``*.routing``: Routing information to reconfigure the anycast networks (sites, neighbor, and bgp attributes)
- ``*.stats``: Catchment statistics




## Routing Configuration (tangler-cli)

As described, we have used different setups and routing configuration policies to perform the experiments. Beside other support tools, we provide the *tangler-cli* tools that manages our testbed using exabgp. You can find more details about it [here](https://github.com/LMBertholdo/TANGLED-cli).

<ul>
<li><code class="language-plaintext highlighter-rouge">tangler-cli.py</code>: used to control BGP routing in an anycast networks
</ul>


#### SYNOPSIS

tangler-cli.py [``-hdv46wrA``] [``--target``=*NODE_TARGET_ADDRESS|all*] [``--cmd``=*ROUTER_COMMAND*] [``-p``=*NUMBER_OF_PREPENDS*] [``-r``=*ROUTE*] [``--version``] 
[``--status``] [``--annouces``] [``--csv``] 
[``--nodes-with-announces``] [``--user``=*USERNAME*] [``--key``=*USER_KEY*]


#### USAGE
```
➜  tangler-cli.py -4 -announces --csv
site,prefix,peer_as,neighbor,attributes
br-poa-anycast02,145.100.118.0/23,264575,177.52.38.113,as-path [ 1149 1149 1149 ]
br-poa-anycast02,145.100.118.0/23,64552,145.100.119.1,as-path [ 1149 1149 1149 ]
nl-ams-anycast01,145.100.118.0/23,64515,169.254.169.254,
us-mia-anycast01,145.100.118.0/23,20080,198.32.252.96,
```



## Measurement Analysis (vp-cli)

The measurements were taken using the tool *Verfploeter (1.0.42)* . *Verfploeter* provides a raw file describing all the catchments and respective metrics. A sample of this raw file is shown below:

```
task_id,client_id,transmit_time,receive_time,send_receive_time_diff,source_address,destination_address,meta_source_address,meta_destination_address,ttl,source_address_country,source_address_asn
17,br-poa-anycast02,1645539507122609845,1645539507242452495,119.84265,1.37.27.39,145.100.118.1,145.100.118.1,1.37.27.39,61,PH,4775
17,br-poa-anycast02,1645539507044239644,1645539507249394694,205.15505,1.1.87.188,145.100.118.1,145.100.118.1,1.1.87.188,243,JP,2519
```

<ul>
<li><code class="language-plaintext highlighter-rouge"> vp-cli.py </code>: used to process Verfploeter raw files to generate the statistics used to build the anycast playbook.
</ul>


#### SYNOPSIS

vp-cli.py [``-hdvq``] [``--file``=*VERFPLOETER_FILE*] [``--hitlist``=*IP_HITLIST_FILE*] 
[``--geo``=*IP_GEOLOCATION_DATABASE*] [``--bgp``=*BGP_POLICY_NAME*] 
[``--weight``=*WEIGHT_FILE*] [``--normalize``] [``--source``] [``--filter``]
[``--nodes-with-announces``]  [``--csv``]   


#### USAGE

```
➜  vp-cli.py --file verfploeter.csv.gz
uk-lnd-anycast02 | 2463339 -  72%  █████████████████████████
br-poa-anycast02 |  769772 -  22%  ███████▋
us-mia-anycast01 |  174477 -   5%  █▋

```



## Visualization Tool (bgp-tuner)

Besides the *vp-cli* interface that provides the load, we've build the GUI called **BGPTuner**
to help on anycast network administration better visualize the effect of anycast redistribution while using a set of playbooks.

<img src="SAND_GUI/gui-screenshot.jpg"
     alt="gui-screenshot"
     style="float: left; margin-right: 10px;" />











## DATASET

Available dataset:

<ul>
<li><code class="language-plaintext highlighter-rouge">Baseline</code>: catchment distribution using regular BGP policies
<li><code class="language-plaintext highlighter-rouge">Prepend</code>: catchment distribution using 1 PREPEND on Miami site
<li><code class="language-plaintext highlighter-rouge">Poisoning</code>: catchment distribution using path poisoning

 
</ul>


#### Python Notebooks Available


Moreover, we make publicity available a set of notebooks that help us to perform our analysis and helped to draw our conclusions:

<ul>
<li><code class="language-plaintext highlighter-rouge">BGP Prepend Investigation</code>: a set of experiments and analysis using BGP routing policy prepend
<li><code class="language-plaintext highlighter-rouge">BGP Path Poisoning</code>: experiments with path poisoning 
<li><code class="language-plaintext highlighter-rouge">Load Distribution</code>: catchment distribution

  
</ul>
