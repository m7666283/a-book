#!/bin/sh/
#
# load_parser
#
# Copyright (C) 2022 by University of Southern California
# Written by ASM Rizvi<asmrizvi@usc.edu>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
#

function usage()
{
    echo "
NAME: 
    load_parser - This script takes input about the data sources, parses captured pcap traces for different routing options (both positive and negative prepending), and generates catchment distribution.
    
Usage: 
    load_parser --numbers=NO_OF_SITES --sites=SITE_LIST --dir=DATA_DIR_BASE --date=EXP_DATE --load=LOAD_FILE_DIR --ldate=LOAD_DATE
    
Sample input: 
    ./load_parser --numbers=3 --sites=BOS,SLC,SEA2 --dir=/nfs/lander/traces/verfploeter/broot_verfploeter/Peering/Peering_Mapping/2020/Path_Prepending_BOS,SLC,SEA2_2020-02-28-HITLIST_20191127/ --date=2020-02-28 --load=/nfs/landerR04/traces/verfploeter/broot_verfploeter/loads/ --ldate=2022-02-06

Options:
    --hitlist <FILE_DIR>      hitlist directory, default is ip_list_20191127.txt 

Args:
    --numbers <NUMBER>        Number of anycast sites
    --sites <LIST>            List of sites separated by comma
    --dir <DIRECTORY>         Parent data source directory
    --date <DATE>             Date in YYYY-MM-DD format
    --load <DIRECTORY>        Load file
    --ldate <DATE>            Load date in YYYY-MM-DD format
"
}

HITLIST="ip_list_20191127.txt"

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        --numbers)
            N_SITES=$VALUE
            ;;
        --sites)
            SITES=$VALUE
            SITE_LIST=(${SITES//,/ })
            ;;
        --dir)
            DEST_DIR=$VALUE
            ;;
        --date)
            DATE=$VALUE
            ;;
         --load)
            LOAD=$VALUE
            ;;
         --ldate)
            LDATE=$VALUE
            ;;
         --hitlist)
            HITLIST=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

for ((index=0;index<$N_SITES;index++));
do
	SITE_CODE=${SITE_LIST[$index]}

	for ((pr_index=0;pr_index<=3;pr_index++));
	do
		OUTPUT_DIR=$DEST_DIR$YEAR-$MONTH-$DAY-$SITES
		if [ $pr_index -ne 0 ]; then
 			OUTPUT_DIR=$OUTPUT_DIR-$SITE_CODE$pr_index
		fi

		if [ ! -d ${OUTPUT_DIR} ] 
		then
			echo "Directory not found..."${OUTPUT_DIR}
		else
			echo "Going: "$OUTPUT_DIR
			#sh load_measurement.sh $N_SITES $SITES $OUTPUT_DIR/ $DATE
			java -jar ParsingLoad.jar $N_SITES $SITES $DEST_DIR/ $OUTPUT_DIR/ $DATE $LOAD/ $LDATE $HITLIST
		fi
	done
done

for ((index=0;index<$N_SITES;index++));
do
	SITE_CODE=${SITE_LIST[$index]}

	for ((pr_index=1;pr_index<=3;pr_index++));
	do
		OUTPUT_DIR=$DEST_DIR$YEAR-$MONTH-$DAY-$SITES
		for ((j=0;j<$N_SITES;j++));
		do
			SITE_CODE_TEMP=${SITE_LIST[$j]}
			if [ "$SITE_CODE_TEMP" != "$SITE_CODE" ]; then
				OUTPUT_DIR=$OUTPUT_DIR-$SITE_CODE_TEMP$pr_index
			fi
		done

		if [ ! -d ${OUTPUT_DIR} ] 
		then
			echo "Directory not found..."${OUTPUT_DIR}
		else
			echo "Going: "${OUTPUT_DIR}
			java -jar ParsingLoad.jar $N_SITES $SITES $DEST_DIR/ $OUTPUT_DIR/ $DATE $LOAD/ $LDATE $HITLIST
		fi
	done
done
