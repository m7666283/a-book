/*
 trafficestimator

 Copyright (C) 2022 by University of Southern California
 Written by ASM Rizvi<asmrizvi@usc.edu>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License,
 version 2, as published by the Free Software Foundation.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 
 */

 /* 
 Description: This code reads the RIPE IPs, parses the incoming traffic (sent by tshark), 
 finds the RIPE IPs within the traffic to get the loss rate, and the estimate the current 
 traffic rate.

 Sample command format: xzcat INPUT.pcap.xz | sudo tshark -r - -T fields -e frame.time_epoch -e ip.src |  
 java -jar TrafficEstimator.jar [RIPE_IP_FILE] [SERVER_IPs] [NORMAL_RATE]

 Sample command: xzcat sample.pcap.xz | sudo tshark -r - -T fields -e frame.time_epoch -e ip.src |  
 java -jar TrafficEstimator.jar ripe-10010-2015-11-30-065034-300s.txt 192.228.79.201,199.9.14.201,2001:500:84::b,2001:500:200::b 31.0 

 Sample output format:
 Time diff: 3.5746731758117676 Count-packets: 9 Rate: 2.517712685148119

 1448866334.106315000 Count-packets: 1613344 Observed rate: 322668.8 Estimated: 5929935.502222222

 Time diff: 5.17244815826416 10 Rate: 1.9333204884852697

 */
package trafficestimator;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class TrafficEstimator {

    public static void printInstruction() {
        System.out.println(" Description: This code reads the RIPE IPs, parses the incoming traffic (sent by tshark), \n"
                + " finds the RIPE IPs within the traffic to get the loss rate, and the estimate the current \n"
                + " traffic rate.\n"
                + "\n"
                + " Sample command format: xzcat INPUT.pcap.xz | sudo tshark -r - -T fields -e frame.time_epoch -e ip.src |  \n"
                + " java -jar TrafficEstimator.jar [RIPE_IP_FILE] [SERVER_IPs] [NORMAL_RATE]\n"
                + "\n"
                + " Sample command: xzcat sample.pcap.xz | sudo tshark -r - -T fields -e frame.time_epoch -e ip.src |  \n"
                + " java -jar TrafficEstimator.jar ripe-10010-2015-11-30-065034-300s.txt 192.228.79.201,199.9.14.201,2001:500:84::b,2001:500:200::b 31.0 \n"
                + "\n"
                + " Sample output format:\n"
                + " Time diff: 3.5746731758117676 Count-packets: 9 Rate: 2.517712685148119\n"
                + "\n"
                + " 1448866334.106315000 Count-packets: 1613344 Observed rate: 322668.8 Estimated: 5929935.502222222\n"
                + "\n"
                + " Time diff: 5.17244815826416 10 Rate: 1.9333204884852697");
    }

    public static void main(final String[] args) throws FileNotFoundException, IOException {
        if (args.length < 2) {
            System.out.println("===============Not enough parameters to run the program=============");
            printInstruction();
            return;
        }
        final HashMap<String, Integer> ipToCount = new HashMap<String, Integer>();

        final FileReader fileReader = new FileReader(args[0]);

        final BufferedReader bufferedReader = new BufferedReader(fileReader);
        String l = "";
        while ((l = bufferedReader.readLine()) != null) {
            ipToCount.put(l.split("\t")[0], 1);
        }

        System.out.println(ipToCount.size());
        final InputStreamReader isReader = new InputStreamReader(System.in);
        final BufferedReader bufReader = new BufferedReader(isReader);
        String line = bufReader.readLine();
        String timeStamp = "";
        String startTime = "";
        String dynamicStartTime = "";
        long dynamicCount = 0;
        ArrayList<String> serverIps = new ArrayList<String>();
        String[] splittedIps = args[1].split(",");
        for (int m = 0; m < splittedIps.length; m++) {
            serverIps.add(splittedIps[m]);
        }

        double diff = 0.0;
        int counterTemp = 0;
        double steadyRate = 1.0;
        if (args.length == 3) {
            steadyRate = Double.parseDouble(args[2]);
        }

        final HashMap<String, Integer> matchedToCount = new HashMap<String, Integer>();
        while ((line = bufReader.readLine()) != null) {
            final String[] splitted = line.split("\t");
            if (splitted.length < 2) {
                continue;
            }

            timeStamp = splitted[0];
            if (startTime.equals("")) {
                startTime = timeStamp;
            }

            final String sourceIp = splitted[1];
            diff = Double.parseDouble(timeStamp) - Double.parseDouble(startTime);
            if (diff < 0.0) {
                continue;
            }

            if (ipToCount.containsKey(sourceIp) && diff <= 240.0) {
                matchedToCount.put(sourceIp, 1);
                counterTemp++;
                if (diff >= 1.0) {
                    System.out.println("Time diff: " + diff + "\tCounter-packets: " + counterTemp + "\tRate: " + (counterTemp / diff));
                }

            }

            if (diff > 240.0) {
                System.out.println("OVER");
                break;
            }

            if (args.length == 3) {
                if (dynamicStartTime.equals("")) {
                    dynamicStartTime = timeStamp;
                }

                if (!serverIps.contains(sourceIp)) {
                    dynamicCount = dynamicCount + 1;
                }

                double newDiff = Double.parseDouble(timeStamp) - Double.parseDouble(dynamicStartTime);
                if (newDiff >= 5.0) {
                    double accessFrac = (counterTemp / diff) / steadyRate;
                    System.out.println(timeStamp + "\tCount-packets: " + dynamicCount + "\tObserved rate: " + (dynamicCount / newDiff) + "\tEstimated: "
                            + ((dynamicCount / accessFrac) / newDiff));
                    dynamicStartTime = timeStamp;
                    dynamicCount = 0;
                }
            }
        }
    }
}
