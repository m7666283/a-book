# Tangled: Catchment Load investigation
### TestBed: Tangled

Experiments performed on Tangled Tested. Several experiments using different routing policies.

## Requeriments
To run this notebook, it's necessary to have Verfploeter raw files. For convenience, we provide a sample in the dataset directory.

```
ls -l ./verfploeter-raw/ 
anycast-prepend-2x-fr-par-anycast01-2019-10-03-05:28:57.csv.gz
anycast-prepend-2x-fr-par-anycast01-2019-10-03-05:28:57.csv.meta
anycast-prepend-2x-fr-par-anycast01-2019-10-03-05:28:57.csv.meta-convert
convert-verfploeter-to-load-file.sh
load-new-prepend-positive-2x-fr-par-1580512204.txt
vp-cli-cmd.sh
```

Convertion

```
vp-cli.py -q -f anycast-prepend-2x-fr-par-anycast01-2019-10-03-05:28:57.csv.gz > load-prepend-2x-fr-par.txt
```
