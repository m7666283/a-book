
# Software pack A-Book (Anycast Playbook) for Tangled Testbed
# RUN-PLAYBOOK

MAKE-PLAYBOOK.py Read all VP-CLI files generated after Verfploter measurements
and generate the Anycast Playbook. Its is possible to generate adding information
from several different measurements, one in each directory.

# make-playbook options

```
usage: ./run-playbook [options]

optional arguments:
  -h, --help   show this help message and exit
  --version    print version and exit
  -v           print verbose messages
  -d, --debug  print debug messages
  --routing    add routing information to playbook file
  --dir DIR    directory with vp-cli stats files
  --out [OUT]  File name to save Playbook results

```

# Make-playbook output

The anycast playbook specifies the percentage of load of each anycast site 
given a specific routing policy. Extra routing information can be added to
playbook by using *--routing* option


## Playbook output
```
#ipv4,br-poa,us-mia,nl-ams
site,prefix,peer_as,neighbor,attributes
br-poa-anycast02,145.100.118.0/23,264575,177.52.38.113,
br-poa-anycast02,145.100.118.0/23,262605,177.184.254.161,
br-poa-anycast02,145.100.118.0/23,64552,145.100.119.1,
nl-ams-anycast01,145.100.118.0/23,64515,169.254.169.254,
us-mia-anycast01,145.100.118.0/23,20080,198.32.252.96,
```

## Example of usage

./make-playbook.py --dir ../datasets/negative-prepend --dir ../datasets/poison --dir ../datasets/prepends --out playbook_X.csv 

