#!/bin/bash
###############################################################################
#  run baseline for anycast testbed
#  Fri Jan 31 18:29:26 CET 2020
#  @copyright ant.isi.edu/paaddos - Leandro Bertholdo - l.m.bertholdo@utwente.nl
#  @copyright sand-project.nl - Joao Ceron - ceron@botlog.org
###############################################################################

shopt -s expand_aliases
###############################################################################
### Program settings
VP_BIN="$HOME/.cargo/bin/verfploeter"
RETVAL=""  #define to global return of verfploeter
ANYCAST_ADDRESS="145.100.118.1"
ANYCAST_PREFIX="145.100.118.0/23"
EXECDIR=`pwd`
ASN_DB="$EXECDIR/tools/GeoLite2-ASN.mmdb"
GEO_DB="$EXECDIR/tools/GeoLite2-Country.mmdb"
TANGLER_CLI="$EXECDIR/tools/tangler-cli.py -4"
HITLIST="$EXECDIR/tools/hitlist.txt"
VPCLI="$EXECDIR/tools/vp-cli.py"
IP2LOCATION="/usr/local/share/ip2location.bin"

#DATE_VAR=`date -u +\%Y-\%m-\%d-\%T`
#DATE_VAR=`date -u +\%Y-\%m-\%d-\%Hh\%Mm`
DATE_PARAM="-u +%Y-%m-%d-%H-%M"
DATE_VAR=`date $DATE_PARAM`

REPO="$EXECDIR/dataset/$DATE_VAR"
#REPO="$EXECDIR/dataset/`date +%Y-%m-%d-%s`"
#REPO="/Volumes/4T/dataset/`date +%Y-%m-%d-%s`"
SLEEP=600
BGP="non"
BIRD="145.100.119.1"


### pinger
PINGER="us-mia-anycast01"
LOG="$REPO/log.txt"

# timestamp
alias utc='date -u +%s'

### ALL Anycast nodes
declare -a NODES
NODES+=("au-syd-anycast01")
NODES+=("br-gru-anycast01")
NODES+=("br-poa-anycast02")
NODES+=("fr-par-anycast01")
NODES+=("uk-lnd-anycast02")
NODES+=("us-sea-anycast01")  
NODES+=("us-los-anycast01")
NODES+=("us-mia-anycast01")
NODES+=("us-was-anycast01")
NODES+=("de-fra-anycast01")  
NODES+=("sg-sin-anycast01")  
NODES+=("dk-cop-anycast01")
NODES+=("za-jnb-anycast01") 
NODES+=("nl-ens-anycast02")
NODES+=("nl-ams-anycast01") 
NODES+=("nl-arn-anycast01")
# #NODES+=("br-gig-anycast01") 
# #NODES+=("jp-hnd-anycast01") 


### HEFICED_nodes
declare -a IXP_HEFICED
#IXP_HEFICED+=("za-jnb-anycast01")  # NAPAfrica


### VULTR_nodes
declare -a IXP_VULTR
IXP_VULTR+=("nl-ams-anycast01")  #IX-AMS
IXP_VULTR+=("au-syd-anycast01")  #IX-AU
IXP_VULTR+=("fr-par-anycast01")  #France-IX
IXP_VULTR+=("uk-lnd-anycast02")  #LINX
IXP_VULTR+=("de-fra-anycast01")  #DEC-IX
IXP_VULTR+=("us-sea-anycast01")  #SIX
IXP_VULTR+=("sg-sin-anycast01")  #EQ-IX


### Direct IXPs using Bird
declare -a IXP_DIRECT
IXP_DIRECT+=("br-gru-anycast01")  #IX-GRU
IXP_DIRECT+=("br-poa-anycast02")  #IX-POA

### BGP Communities VULTR_nodes
declare -a VCOMM
#VCOMM+=("20473:6601")   #no-ix just VULTR private peering
#VCOMM+=("64600:6000")   #no-export
#VCOMM+=("64600:63956")  #no-ix-au
VCOMM+=("64600:2914")    #no-ntt
VCOMM+=("64600:1299")    #no-telia
VCOMM+=("64600:3257")    #no-gtt
VCOMM+=("64600:3356")    #no-centurylink
VCOMM+=("64600:174")     #no-cogent
VCOMM+=("64600:7922")    #no-comcast
VCOMM+=("64600:63956")   #no-equinix-AU
VCOMM+=("64600:3491")    #no-PCCW Singapure
VCOMM+=("64600:2516")    #no-KDDI Tokyo 
VCOMM+=("64600:9318")    #no-SKB South-Corea KINX
VCOMM+=("0:1103")        #no surfnet on AMSIX
VCOMM+=("0:2603")        #no nordunet on AMSIX
VCOMM+=("64600:2603")    #no nordunet on VULTR private peering


### BGP Communities HEFICED_nodes
declare -a HCOMM
HCOMM+=("61317:65100")   #no-telia
HCOMM+=("61317:65110")   #no-gtt
HCOMM+=("61317:65120")   #no-pccw
HCOMM+=("61317:65130")   #no-ntt
HCOMM+=("61317:65140")   #no-centurylink
HCOMM+=("61317:65150")   #no-servercentral
HCOMM+=("61317:65160")   #no-sparkle
HCOMM+=("61317:65170")   #no-liquid
HCOMM+=("61317:65180")   #no-wiocc
HCOMM+=("61317:65190")   #no-DECIX-Frankfurt
HCOMM+=("61317:65200")   #no-DECIX-Dusseldorf
HCOMM+=("61317:65210")   #no-IX.br
#HCOMM+=("61317:65230")   #no-NAPAfrica
HCOMM+=("61317:65230")   #no-JINX

### BGP Communities IX.br
declare -a BRCOMM
BRCOMM+=("65000:1251")   #no-ansp
BRCOMM+=("65000:20080")  #no-ampath
BRCOMM+=("65000:264575") #Nexfibra
BRCOMM+=("65000:262605") #i7provider.com.br


# ###############################################################################
# ### Functions
# # ----------------------------------------------------------------------------
# #source 00-functions.sh
# 
# 
# ###############################################################################
# ### MAIN
# ###############################################################################
# 
# # create repository dir
# [ ! -d $REPO ] && mkdir $REPO
# 
# # update symlink
# dir_=`dirname $REPO`
# ln -sfn $REPO $dir_/last
# 
# echo "logfile in: $LOG"
# ## initial setup
# date | tee -a $LOG
# 
# # check the VPN Tunnel
# check_tunnel
# 
# # show defined nodes
# show_nodes | tee -a $LOG
# 
# # show verfploeter nodes connected
# $VP_BIN cli client-list | tee -a $LOG
# 
# 
# ############################################################
# ## Anycast distribution - regular monitoring - BASELINE
# ############################################################
# 
# # this will withdraw all the prefix only on the selected nodes (@NODES)
# #withdraw_all_running_nodes
# # this will withdraw all the prefix over all the nodes
# echo "removing all routes"
# $TANGLER_CLI -4 -w
# #sleep 30
# 
# echo "-------"
# echo " anycast baseline started: `date` " | tee -a $LOG
# echo "-------"
# 
# # add prefix
# echo "Announcing"
# #set_announces_in_all_nodes
# advertise_on_all_nodes "145.100.118.0/24"
# 
# ACTIVE_BGP_NODES=$($TANGLER_CLI -4 --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")
# BGP="baseline-$ACTIVE_BGP_NODES"
# OUTFILE="$REPO/anycast-baseline-$ACTIVE_BGP_NODES-`date -u +\%Y-\%m-\%d-\%T`.csv"
# 
# echo "-------"
# echo " We are generating packet in: $PINGER " | tee -a $LOG | tee -a $OUTFILE.stats
# echo " We set this BGP Policy: $BGP " | tee -a $LOG | tee -a $OUTFILE.stats
# echo "-------"
# 
# run_verfloeter $OUTFILE $PINGER
# 
# echo "-------"
# echo "finished baseline monitoring"
# echo "-------"
# echo
#     
# 