#!/bin/bash
###############################################################################
#  run verfploeter
#  Fri Feb 28 10:31:12 CET 2020
#  @copyright ant.isi.edu/paaddos - Leandro Bertholdo - l.m.bertholdo@utwente.nl
#  @copyright sand-project.nl - Joao Ceron - ceron@botlog.org
###############################################################################

shopt -s expand_aliases
###############################################################################
### Program settings
VERFPLOETER_BIN="$HOME/.cargo/bin/verfploeter"
ANYCAST_ADDRESS="145.100.118.1"
ANYCAST_PREFIX="145.100.118.0/23"
EXECDIR=`pwd`
ASN_DB="$EXECDIR/tools/GeoLite2-ASN.mmdb"
GEO_DB="$EXECDIR/tools/GeoLite2-Country.mmdb"
TANGLER_CLI="$EXECDIR/tools/tangler-cli.py"
HITLIST="$EXECDIR/tools/hitlist.txt"

### pinger
ORIGIN="us-mia-anycast01"
#ORIGIN="dk-cop-anycast01"

# ORIGIN="nl-ens-anycast02"
#ORIGIN="za-jnb-anycast01"

#ORIGIN+=("au-syd-anycast01")
#ORIGIN+=("br-gru-anycast01")
#ORIGIN+=("br-poa-anycast02")
#ORIGIN+=("fr-par-anycast01")
#ORIGIN+=("jp-hnd-anycast01")
#ORIGIN+=("nl-ens-anycast02")
#ORIGIN+=("uk-lnd-anycast02")
#ORIGIN+=("us-los-anycast01")
#ORIGIN+=("us-mia-anycast01")
#ORIGIN+=("us-was-anycast01")
#ORIGIN+=("nl-ams-anycast01")  
#ORIGIN+=("de-fra-anycast01")  
#ORIGIN+=("us-sea-anycast01")  
#ORIGIN+=("sg-sin-anycast01")  
#ORIGIN+=("za-jnb-anycast01") 
#ORIGIN+=("br-gig-anycast01") 

### node
#NODE="za-jnb-anycast01"

NODES+=("au-syd-anycast01")
NODES+=("br-gru-anycast01")
NODES+=("br-poa-anycast02")
NODES+=("fr-par-anycast01")
NODES+=("jp-hnd-anycast01")
NODES+=("nl-ens-anycast02")
NODES+=("uk-lnd-anycast02")
NODES+=("us-los-anycast01")
NODES+=("us-mia-anycast01")
NODES+=("us-was-anycast01")
NODES+=("nl-ams-anycast01")  
NODES+=("de-fra-anycast01")  
NODES+=("us-sea-anycast01")  
NODES+=("sg-sin-anycast01")  
#NODES+=("za-jnb-anycast01") 

#NODES+=("br-gig-anycast01") 

declare -a VCOMM
#VCOMM+=("20473:6601")   #no-ix just VULTR private peering
#VCOMM+=("64600:6000")   #no-export
#VCOMM+=("64600:63956")  #no-ix-au
VCOMM+=("64600:2914")    #no-ntt
VCOMM+=("64600:1299")    #no-telia
VCOMM+=("64600:3257")    #no-gtt
VCOMM+=("64600:3356")    #no-centurylink
VCOMM+=("64600:174")     #no-cogent
VCOMM+=("64600:7922")    #no-comcast
VCOMM+=("64600:63956")   #no-equinix-AU
VCOMM+=("64600:3491")    #no-PCCW Singapure
VCOMM+=("64600:2516")    #no-KDDI Tokyo 
VCOMM+=("64600:9318")    #no-SKB South-Corea KINX
VCOMM+=("0:1103")        #no surfnet on AMSIX
VCOMM+=("0:2603")        #no nordunet on AMSIX
VCOMM+=("64600:2603")    #no nordunet on VULTR private peering
#VCOMM+=("64600:2914 64600:1299 64600:3257 64600:3356 64600:7922 64600:174 64600:3491 64600:63956")
# [ 0:1103 0:2603 20473:6601 64600:174 64600:1299 64600:2603 64600:2914 64600:3257 64600:3356 64600:3491 64600:7922 64600:63956 ]


declare -a HCOMM
HCOMM+=("61317:65100 61317:65110 61317:65120 61317:65130 61317:65140 61317:6510 61317:65160 61317:65170 61317:65180 61317:65190 61317:65200 61317:65210 61317:65230")


# timestamp
alias utc='date -u +%s'

###############################################################################
### Function
# ----------------------------------------------------------------------------
check_tunnel(){
    # check ssh tunnel
    # we have an overlay net (vpn) to collect the results from vp nodes
    result=`ps gawx | grep ssh | grep 50001 | wc -l`
    if [ "$result" -lt 1 ]; then
        echo "setting tunnel ssh to master"
        ssh -f  -N  -L 50001:localhost:50001 139.162.152.35
    fi
}

set_announces_in_all_nodes(){
    # add regular announcement for all the nodes
    for NODE in "${NODES[@]}"
    do
        # add prefix announcement for $NODE
        echo "add prefix announcement for $NODE"
        echo "-------"
        $TANGLER_CLI  -t $NODE -A -r $ANYCAST_PREFIX
    done
    echo "I'll sleep for [$SLEEP]sec to wait for the BGP converge"
    sleep $SLEEP
}
# ----------------------------------------------------------------------------
###############################################################################
### MAIN
###############################################################################

# check the VPN Tunnel
echo "-------------------------------------------------------------------------"
echo "Checking SSH Tunnel to Master"
check_tunnel
echo

echo "-------------------------------------------------------------------------"
echo "Checking Verfploeter connection"
# show verfploeter nodes connected
$VERFPLOETER_BIN cli client-list | tee -a $LOG
echo

echo "-------------------------------------------------------------------------"
echo "Checking Routing on Tangled"

#set_announces_in_all_nodes

# set specific test /23 ENS /24 AMS No-IXP for leak prefix finding
$TANGLER_CLI  -A -r 145.100.118.0/23 -t nl-ens-anycast02
#$TANGLER_CLI  -4 -t nl-ams-anycast01 -c "announce route 145.100.118.0/24 next-hop self community [ 0:6777 20473:6601  0:1103 0:2603 64600:2603 64600:2914 64600:1299 64600:3257 64600:3356 64600:7922 64600:174 64600:3491 64600:63956 ]"
$TANGLER_CLI  -4 -t nl-ams-anycast01  -c "announce route 145.100.118.0/24 next-hop self community [ ${VCOMM[*]} ]"
$TANGLER_CLI -a -4 -t $NODE

# run verfploeter
echo "Running verfploeter with ORIGIN [$ORIGIN] ouotput /tmp/01-test.txt"
$VERFPLOETER_BIN cli start $ORIGIN $ANYCAST_ADDRESS  $HITLIST  -a $ASN_DB -c $GEO_DB >  /tmp/01-test.txt 
echo "DONE!"
echo "Output at /tmp/01-test.txt "


exit 1




echo "Clearing all announcement.. " | tee -a $LOG
#$TANGLER_CLI -4 -w
echo "Setting Pinger on [$ORIGIN]"
echo "Setting announcement on [$NODE] " | tee -a $LOG
#$TANGLER_CLI  -4 -t $NODE -A -r 145.100.119.0/24
echo "$TANGLER_CLI  -4 -t $NODE -c \"announce route $ANYCAST_PREFIX next-hop self\"" # community [$HCOMM]"
$TANGLER_CLI  -4 -t $NODE -c "announce route $ANYCAST_PREFIX next-hop self "
#$TANGLER_CLI  -4 -t $NODE -c "announce route $ANYCAST_PREFIX next-hop self  community [$HCOMM]" # community [$HCOMM]"
echo
echo "Showing announcement.. " | tee -a $LOG
echo "Normal announcement"
$TANGLER_CLI -a -4 -t $NODE
#echo "Announcement with community (IF expected) [$HCOMM]"
ssh za-jnb-anycast01 "exabgpcli sh adj-rib out extensive"
echo
echo
echo "Sleep 120s"
sleep 120
# run verfploeter
echo "Running verfploeter with ORIGIN [$ORIGIN]"
$VERFPLOETER_BIN cli start $ORIGIN $ANYCAST_ADDRESS  $HITLIST  -a $ASN_DB -c $GEO_DB | tee  /tmp/01-test.txt 

# Remove announcement
echo "Remove Test announcement.. " | tee -a $LOG
#$TANGLER_CLI  -4  -w 
$TANGLER_CLI  -t $NODE -c "withdraw route $ANYCAST_PREFIX"



