#!/bin/bash
###############################################################################
#  run baseline for anycast testbed
#  Fri Jan 31 18:29:26 CET 2020
#  @copyright ant.isi.edu/paaddos - Leandro Bertholdo - l.m.bertholdo@utwente.nl
#  @copyright sand-project.nl - Joao Ceron - ceron@botlog.org
###############################################################################
# include functions and aliases
source 00-functions.sh
shopt -s expand_aliases
###############################################################################
### Program settings
VP_BIN="$HOME/.cargo/bin/verfploeter"
ANYCAST_ADDRESS="145.100.118.1"
ANYCAST_PREFIX="145.100.118.0/23"
EXECDIR=`pwd`
ASN_DB="$EXECDIR/tools/GeoLite2-ASN.mmdb"
GEO_DB="$EXECDIR/tools/GeoLite2-Country.mmdb"
TANGLER_CLI="$EXECDIR/tools/tangler-cli.py -4"
HITLIST="$EXECDIR/tools/hitlist.txt"
VPCLI="$EXECDIR/tools/vp-cli.py"
IP2LOCATION="/usr/local/share/ip2location.bin"
#DATE_VAR=`date -u +\%Y-\%m-\%d-\%T`
DATE_VAR=`date -u +\%Y-\%m-\%d-\%Hh\%Mm`
REPO="$EXECDIR/dataset/`date -u +%Y-%m-%d-%Hh\%Mm`"
#REPO="/Volumes/4T/dataset/`date +%Y-%m-%d-%s`"
BGP="baseline"
SLEEP=600
LOG="$REPO/log.txt"

### pinger
PINGER="us-was-anycast01"

# timestamp
alias utc='date -u +%s'

# anycast testbed nodes
declare -a NODES
#NODES+=("au-syd-anycast01")
#NODES+=("br-gru-anycast01")
NODES+=("br-poa-anycast02")
#NODES+=("fr-par-anycast01")
#NODES+=("jp-hnd-anycast01")
#NODES+=("nl-ens-anycast02")
#NODES+=("uk-lnd-anycast02")
#NODES+=("us-los-anycast01")
NODES+=("us-mia-anycast01")
#NODES+=("us-was-anycast01")
NODES+=("nl-ams-anycast01")  
#NODES+=("de-fra-anycast01")  
#NODES+=("us-sea-anycast01")  
#NODES+=("sg-sin-anycast01")  
#NODES+=("za-jnb-anycast01") 

###############################################################################
### MAIN
###############################################################################
# create repository dir
echo "Creating repository [$REPO] " 
[ ! -d $REPO ] && mkdir $REPO

# update symlink
dir_=`dirname $REPO`
ln -sfn $REPO $dir_/last

echo "logfile in: $LOG"
## initial setup
date | tee -a $LOG

# check the VPN Tunnel
echo "Checking VPN Tunnel" | tee -a $LOG
check_tunnel

# show defined nodes
echo "Show defined nodes" | tee -a $LOG
show_nodes | tee -a $LOG

# show verfploeter nodes connected
echo "show verfploeter cli client-list" | tee -a $LOG
$VP_BIN cli client-list | tee -a $LOG

############################################################
## Anycast distribution - regular monitoring - BASELINE
############################################################
# this will withdraw all the prefix over all the nodes
echo "withdraw_all_running_nodes" | tee -a $LOG
$TANGLER_CLI -4 -6 -w

echo "-------"
echo " anycast baseline started: `date` " | tee -a $LOG
echo "-------"

# add prefix
advertise_on_all_nodes $ANYCAST_PREFIX

# check active nodes
ACTIVE_BGP_NODES=$($TANGLER_CLI -4 --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")
BGP="baseline-$ACTIVE_BGP_NODES"
OUTFILE="$REPO/$BGP"

echo "-------"
echo " We are generating packet in: $PINGER " | tee -a $LOG 
echo " We set this BGP Policy: $BGP " | tee -a $LOG 
echo "-------"
run_verfploeter_looped  $OUTFILE  $PINGER

echo "-------"
echo "finished baseline monitoring"
echo "-------"
echo
    
