#!/bin/bash
###############################################################################
# run experiments on verfploeter for catchment manipulation
#  Fri Jan 31 18:29:26 CET 2020
#  @copyright ant.isi.edu/paaddos - Leandro Bertholdo - l.m.bertholdo@utwente.nl
#  @copyright sand-project.nl - Joao Ceron - ceron@botlog.org
###############################################################################

shopt -s expand_aliases
###############################################################################
### Program settings
VERFPLOETER_BIN="$HOME/.cargo/bin/verfploeter"
ANYCAST_ADDRESS="145.100.118.1"
ANYCAST_PREFIX="145.100.118.0/23"
EXECDIR=`pwd`
ASN_DB="$EXECDIR/tools/GeoLite2-ASN.mmdb"
GEO_DB="$EXECDIR/tools/GeoLite2-Country.mmdb"
TANGLER_CLI="$EXECDIR/tools/testbed-cli.py -4"
HITLIST="$EXECDIR/tools/hitlist.txt"
VPCLI="$EXECDIR/tools/vp-cli.py"
IP2LOCATION="/usr/local/share/ip2location.bin"
DATE_VAR=`date -u +\%Y-\%m-\%d-\%T`
REPO="$EXECDIR/dataset/`date +%Y-%m-%d-%s`"
REPO="/Volumes/4T/dataset/`date +%Y-%m-%d-%s`"
BGP="non"

### pinger 
ORIGIN="us-mia-anycast01"

LOG="$REPO/log.txt"

# timestamp
alias utc='date -u +%s'

# anycast testbed nodes
declare -a NODES
NODES+=("us-was-anycast01")
NODES+=("us-mia-anycast01")
NODES+=("br-poa-anycast02")
NODES+=("jp-hnd-anycast01")
NODES+=("au-syd-anycast01")
NODES+=("uk-lnd-anycast02")
NODES+=("fr-par-anycast01")

## used to build filename
#ACTIVE_BGP_NODES=$($TANGLER_CLI -4 --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")
#echo $ACTIVE_BGP_NODES

###############################################################################
### Functions

# ----------------------------------------------------------------------------
# show nodes used in this experiment
show_nodes(){

    echo "we are using ${#NODES[@]} nodes in this experiment"; 
    for NODE in "${NODES[@]}"
    do
        echo "$NODE"
    done
}

# ----------------------------------------------------------------------------
# run verfploeter and collect results
run_verfloeter(){
   OUTFILE=$1
   [ ! -d $REPO ] && mkdir $REPO
   echo $OUTFILE
   echo "$VERFPLOETER_BIN cli start $ORIGIN $ANYCAST_ADDRESS  $HITLIST  -a $ASN_DB -c $GEO_DB  > $OUTFILE"
   $VERFPLOETER_BIN cli start $ORIGIN $ANYCAST_ADDRESS  $HITLIST  -a $ASN_DB -c $GEO_DB  > $OUTFILE
   numlines=$(wc -l $OUTFILE | awk '{print $1}')
   if [ "$numlines" -gt 3000000 ]; then
            echo "-s $NODE -v -b \"$BGP\" -f \"$OUTFILE\" " > $OUTFILE.meta
            $TANGLER_CLI --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g" > $OUTFILE.routing
            $TANGLER_CLI -a --csv >> $OUTFILE.routing

            # stats
            $VPCLI -f $OUTFILE  -q  > $OUTFILE.stats  

            gzip $OUTFILE &
            result="OK"

   else
        echo "output presented a low number of lines [$numlines]"
        result="ERROR"
        gzip  $OUTFILE &
   fi
   # build convertion line
   echo "$VPCLI -s $ORIGIN  -b \"$BGP\" -g $IP2LOCATION  -f $OUTFILE.gz -n -q " > $OUTFILE.meta-convert

}

# ----------------------------------------------------------------------------
check_tunnel(){
    # check ssh tunnel
    # we have an overlay net (vpn) to collect the results from vp nodes
    result=`ps gawx | grep ssh | grep 50001 | wc -l`
    if [ "$result" -lt 1 ]; then
        echo "setting tunnel ssh to master"
        ssh -f  -N  -L 50001:localhost:50001 master
    fi
}

# ----------------------------------------------------------------------------
set_announces_in_all_nodes(){
    # add regular announcement for all the nodes
    for NODE in "${NODES[@]}"
    do
        # add prefix announcement for $NODE
        echo "add prefix announcement for $NODE"
        echo "-------"
        $TANGLER_CLI  -t $NODE -A -r $ANYCAST_PREFIX 
    done

}
# ----------------------------------------------------------------------------
withdraw_all_running_nodes(){
    echo "Cleaning all the announcements"
    # withdraw routes from all the nodes
    for NODE in "${NODES[@]}"
    do
        $TANGLER_CLI -t $NODE -w  -4
    done
    sleep 10
}

# ----------------------------------------------------------------------------
add_routes_to_node(){
    curr_node=$1
    echo "-------"
    echo "add prefix announcement for $curr_node" | tee -a $LOG
    echo "-------"
    $TANGLER_CLI -4 -t $curr_node  -A -r "145.100.118.0/23"
}

# ----------------------------------------------------------------------------
remove_route_from_node(){
    curr_node=$1
    echo "-------"
    echo "remove announcement from $curr_node" | tee -a $LOG
    echo "-------"
    $TANGLER_CLI -4 -t $curr_node  -w 
}

###############################################################################
### MAIN
###############################################################################

# create repository dir 
[ ! -d $REPO ] && mkdir $REPO

# update symlink
dir_=`dirname $REPO`
#ln -sfn $REPO $dir_/last

echo "logfile in: $LOG"
## initial setup
date | tee -a $LOG

# check the VPN Tunnel
check_tunnel

# show defined nodes
echo "my anycast is composed by these nodes:"
show_nodes

# show verfploeter nodes connected
echo "this is my verfploeter clients"
$VERFPLOETER_BIN cli client-list

withdraw_all_running_nodes
set_announces_in_all_nodes
echo "-------"
echo "anycast network started: `date` " | tee -a $LOG
echo "-------"
$TANGLER_CLI -a --csv & 
echo "anycast UP: `date` " | tee -a $LOG
sleep 30


# ----------------------------------------------------------------------------
set_announces_on_ampath(){

    com=$1

    # anycast testbed nodes
    declare -a NODES_ampath
    NODES_ampath+=("us-mia-anycast01")


    for NODE in "${NODES_ampath[@]}"
    do
        # add prefix announcement for $NODE
        echo "add prefix announcement for $NODE - com $com"
        echo "-------"
        $TANGLER_CLI  -4 -t $NODE -c "announce route $ANYCAST_PREFIX  next-hop self community [$community]"

    done
}


# ----------------------------------------------------------------------------
withdraw_all_vultr_nodes(){

    # anycast testbed nodes
    declare -a NODES_ampath
    NODES_ampath+=("us-mia-anycast01")


    echo "Cleaning community announcements from VULTR"
    for NODE in "${NODES_ampath[@]}"
    do
        # add prefix announcement for $NODE
        echo "remove community from ampath  $NODE "
        echo "-------"
        #add_routes_to_node $NODE
        $TANGLER_CLI -t $NODE -A -r $ANYCAST_PREFIX
    done
}

#----------------------------------------------------------
# 2 - request prepending on ampath
#----------------------------------------------------------

# https://ampath.net/AMPATH_BGP_Policies.php
# communities
declare -a COMM
COMM+=("20080:801") # prepend 1x
COMM+=("20080:802") # prepend 2x
COMM+=("20080:803") # prepend 3x
COMM+=("20080:804") # prepend 4x

for community in "${COMM[@]}"
do
    set_announces_on_ampath $community
    $TANGLER_CLI -4 -a --csv
    sleep 600
    num_prepend=`echo "${community: -1}"`

    ## run VERFPLOETER
    ACTIVE_BGP_NODES=$($TANGLER_CLI --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")
    OUTFILE="$REPO/anycast-prepend-usingcommunity-$num_prepend"x"-ampathnodes-$ACTIVE_BGP_NODES-$DATE_VAR.csv"
    BGP="prepend-usingcommunity-$num_prepend"x"-ampath-nodes"

    run_verfloeter $OUTFILE
    withdraw_all_vultr_nodes
done


#----------------------------------------------------------
# 3 - traditional prepending
#----------------------------------------------------------

# ----------------------------------------------------------------------------
set_regular_announces_on_ampath(){

    prepend=$1

    # anycast testbed nodes
    declare -a NODES_ampath
    NODES_ampath+=("us-mia-anycast01")

    for NODE in "${NODES_ampath[@]}"
    do
        # add prefix announcement for $NODE
        echo "add prefix announcement for $NODE - prepend $prepend"
        echo "-------"
        $TANGLER_CLI  -4 -t $NODE -A -r $ANYCAST_PREFIX -P $prepend

    done
}
# ----------------------------------------------------------------------------


NUM_PREPEND=4
for num_prepend in `seq $NUM_PREPEND`;

do
    set_regular_announces_on_ampath $num_prepend 
    $TANGLER_CLI -4 -a --csv
    sleep 600
    ## run VERFPLOETER
    ACTIVE_BGP_NODES=$($TANGLER_CLI --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")

    OUTFILE="$REPO/anycast-prepend-regular-$num_prepend"x"-ampathnodes-$ACTIVE_BGP_NODES-$DATE_VAR.csv"
    BGP="prepend-regular_positive-$num_prepend"x"-ampath-nodes"
    run_verfloeter $OUTFILE
    withdraw_all_vultr_nodes
done

exit

