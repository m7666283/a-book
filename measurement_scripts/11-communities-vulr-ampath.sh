#!/bin/bash
###############################################################################
#  run baseline for anycast testbed
#  Fri Jan 31 18:29:26 CET 2020
#  @copyright ant.isi.edu/paaddos - Leandro Bertholdo - l.m.bertholdo@utwente.nl
#  @copyright sand-project.nl - Joao Ceron - ceron@botlog.org
###############################################################################

shopt -s expand_aliases
###############################################################################
### Program settings
VERFPLOETER_BIN="$HOME/.cargo/bin/verfploeter"
ANYCAST_ADDRESS="145.100.118.1"
ANYCAST_PREFIX="145.100.118.0/23"
EXECDIR=`pwd`
ASN_DB="$EXECDIR/tools/GeoLite2-ASN.mmdb"
GEO_DB="$EXECDIR/tools/GeoLite2-Country.mmdb"
TANGLER_CLI="$EXECDIR/tools/tangler-cli.py -4"
HITLIST="$EXECDIR/tools/hitlist.txt"
VPCLI="$EXECDIR/tools/vp-cli.py"
IP2LOCATION="/usr/local/share/ip2location.bin"
DATE_VAR=`date -u +\%Y-\%m-\%d-\%T`
REPO="$EXECDIR/dataset/`date +%Y-%m-%d-%s`"
BGP="non"
LOG="$REPO/log.txt"
SLEEP=60

### pinger
PINGADOR="dk-cop-anycast01"
declare -a ORIGINS
#ORIGINS+=("us-mia-anycast01")
#ORIGINS+=("us-was-anycast01")
#ORIGINS+=("br-gru-anycast01")
#ORIGINS+=("br-poa-anycast01")
#ORIGINS+=("au-syd-anycast01")
#ORIGINS+=("fr-par-anycast01")
#ORIGINS+=("jp-hnd-anycast01")
#ORIGINS+=("nl-ens-anycast02")
#ORIGINS+=("uk-lnd-anycast02")
ORIGINS+=("dk-cop-anycast01")
#ORIGINS+=("us-los-anycast01")


# timestamp
alias utc='date -u +%s'

# anycast testbed nodes
declare -a NODES
#NODES+=("au-syd-anycast01")
#NODES+=("br-gru-anycast01")
NODES+=("br-poa-anycast01")
#NODES+=("fr-par-anycast01")
#NODES+=("jp-hnd-anycast01")
#NODES+=("nl-ens-anycast02")
NODES+=("uk-lnd-anycast02")
#NODES+=("us-los-anycast01")
NODES+=("us-mia-anycast01")
#NODES+=("us-was-anycast01")

###############################################################################
### Functions
# ----------------------------------------------------------------------------
check_tunnel(){
    # check ssh tunnel
    # we have an overlay net (vpn) to collect the results from vp nodes
    result=`ps gawx | grep ssh | grep 50001 | wc -l`
    if [ "$result" -lt 1 ]; then
        echo "setting tunnel ssh to master"
        ssh -f  -N  -L 50001:localhost:50001 master
    fi
}

# ----------------------------------------------------------------------------
# show nodes used in this experiment
show_nodes(){
    echo "we are using ${#NODES[@]} nodes in this experiment";
    for NODE in "${NODES[@]}"
    do
        echo "$NODE"
    done
}
# ----------------------------------------------------------------------------
add_routes_to_node(){
    curr_node=$1
    echo "-------"
    echo "add prefix announcement for $curr_node" | tee -a $LOG
    echo "-------"
    $TANGLER_CLI -4 -t $curr_node  -A -r $ANYCAST_PREFIX
}

# ----------------------------------------------------------------------------
remove_route_from_node(){
    curr_node=$1
    echo "-------"
    echo "remove announcement from $curr_node" | tee -a $LOG
    echo "-------"
    $TANGLER_CLI -4 -t $curr_node  -w
}

# ----------------------------------------------------------------------------
# run verfploeter and collect results
run_verfloeter(){
   OUTFILE=$1
   ORIGIN=$2
   ORIGIN=$PINGADOR

   [ ! -d $REPO ] && mkdir $REPO
   echo $OUTFILE
   echo "$VERFPLOETER_BIN cli start $ORIGIN $ANYCAST_ADDRESS  $HITLIST  -a $ASN_DB -c $GEO_DB  > $OUTFILE"
   $VERFPLOETER_BIN cli start $ORIGIN $ANYCAST_ADDRESS  $HITLIST  -a $ASN_DB -c $GEO_DB  > $OUTFILE
   numlines=$(wc -l $OUTFILE | awk '{print $1}')
   if [ "$numlines" -gt 3000000 ]; then
            echo "-s $NODE -v -b \"$BGP\" -f \"$OUTFILE\" " > $OUTFILE.meta
            $TANGLER_CLI --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g" > $OUTFILE.routing
            $TANGLER_CLI -a --csv >> $OUTFILE.routing

            # stats
            $VPCLI -f $OUTFILE  -q >> $OUTFILE.stats

            gzip $OUTFILE &
            result="OK"

   else
        echo "output presented a low number of lines [$numlines]"
        result="ERROR"
        gzip  $OUTFILE &
   fi
   # build convertion line
   echo "$VPCLI -s $ORIGIN  -b \"$BGP\" -g $IP2LOCATION  -f $OUTFILE.gz -n -q " > $OUTFILE.meta-convert
   echo
}

# ----------------------------------------------------------------------------
set_announces_in_all_nodes(){
    # add regular announcement for all the nodes
    for NODE in "${NODES[@]}"
    do
        # add prefix announcement for $NODE
        echo "add prefix announcement for $NODE"
        echo "-------"
        echo "$TANGLER_CLI  -t $NODE -A -r $ANYCAST_PREFIX"
        $TANGLER_CLI  -t $NODE -A -r $ANYCAST_PREFIX
    done
    $TANGLER_CLI -4 -a --csv
    echo "I'll sleep for" $SLEEP "seconds to wait for the BGP converge"
    sleep $SLEEP
}
# ----------------------------------------------------------------------------
withdraw_all_running_nodes(){
     echo "Cleaning all the announcements"
     # withdraw routes from all the nodes
     for NODE in "${NODES[@]}"
     do
         $TANGLER_CLI -t $NODE -w  -4
     done
     sleep $SLEEP
}

# ----------------------------------------------------------------------------
add_routes_to_node(){
    curr_node=$1
    echo "-------"
    echo "add prefix announcement for $curr_node" | tee -a $LOG
    echo "-------"
    $TANGLER_CLI -4 -t $curr_node  -A -r "145.100.118.0/23"
    }
        
# ----------------------------------------------------------------------------
remove_route_from_node(){
    curr_node=$1
    echo "-------"
    echo "remove announcement from $curr_node" | tee -a $LOG
    echo "-------"
    $TANGLER_CLI -4 -t $curr_node  -w
}

# ----------------------------------------------------------------------------
running_for_several_origins(){
    for origin in "${ORIGINS[@]}"
    do
        ACTIVE_BGP_NODES=$($TANGLER_CLI -4 --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")
        BGP="baseline-$ACTIVE_BGP_NODES"
        OUTFILE="$REPO/FROM-$origin-TO-$ACTIVE_BGP_NODES-`date -u +\%Y-\%m-\%d-\%T`.csv"
        
        echo "-------"
        echo " We are generating packet in: $origin " | tee -a $LOG | tee -a $OUTFILE.stats
        echo " We set this BGP Policy: $BGP " | tee -a $LOG | tee -a $OUTFILE.stats
        echo 
        run_verfloeter $OUTFILE $PINGADOR
    
        # wait +1min to verfploeter finish
        sleep $SLEEP
    done
        
    echo "-------"
    echo "finished anycast experiment"
    echo "-------"
    echo
}
###############################################################################
### MAIN
###############################################################################

# create repository dir
[ ! -d $REPO ] && mkdir $REPO

# update symlink
dir_=`dirname $REPO`
ln -sfn $REPO $dir_/last

echo "logfile in: $LOG"
## initial setup
date | tee -a $LOG

# check the VPN Tunnel
check_tunnel

# show defined nodes
show_nodes | tee -a $LOG

# show verfploeter nodes connected
$VERFPLOETER_BIN cli client-list | tee -a $LOG

#----------------------------------------------------------
# 1 - Anycast distribution - regular monitoring - BASELINE
#----------------------------------------------------------
echo "-----------------------------------------------------------"
echo "anycast baseline experiment started: `date` " | tee -a $LOG
echo "-------"

$TANGLER_CLI -4 -w 
set_announces_in_all_nodes
ACTIVE_BGP_NODES=$($TANGLER_CLI -4 --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")
BGP="baseline-$ACTIVE_BGP_NODES"
OUTFILE="$REPO/anycast-baseline-$ACTIVE_BGP_NODES-$DATE_VAR.csv"

#echo "-------"
#echo " We are generating packet in: $PINGADOR " | tee -a $LOG | tee -a $OUTFILE.stats
#echo " We set this BGP Policy: $BGP " | tee -a $LOG | tee -a $OUTFILE.stats
#echo
##run_verfloeter $OUTFILE $origin
# wait +1min to verfploeter finish
#sleep 30
#
#echo "-------"
#echo "anycast baseline experiment finished: `date` " | tee -a $LOG
#echo "-------"
#echo

#----------------------------------------------------------
# 2 - Change Vultr at UK-LND
#----------------------------------------------------------
echo "-----------------------------------------------------------"
echo "anycast service is ready for VULTR communities experiment: `date` " | tee -a $LOG
echo "-------"

# communities
declare -a COMM
# COMM+=("20473:6601")    #no-ix
# COMM+=("20473:6601 64601:2914")    #ntt 1-prepend
# COMM+=("20473:6601 64602:2914")    #ntt 2-prepend
# COMM+=("20473:6601 64603:2914")    #ntt 3-prepend
# COMM+=("20473:6601 64600:2914")    #ntt no-export
# COMM+=("20473:6601 64600:2914 64601:1299")    #telia 1-prep
# COMM+=("20473:6601 64600:2914 64602:1299")    #telia 2-prep
# COMM+=("20473:6601 64600:2914 64603:1299")    #telia 3-prep
# COMM+=("20473:6601 64600:2914 64600:1299")    #telia no-export
# COMM+=("20473:6601 64600:2914 64600:1299 64601:3356")    #level3
# COMM+=("20473:6601 64600:2914 64600:1299 64602:3356")    #level3
# COMM+=("20473:6601 64600:2914 64600:1299 64603:3356")    #level3
# COMM+=("20473:6601 64600:2914 64600:1299 64600:3356")    #level3
# COMM+=("20473:6601 64600:2914 64600:1299 64600:3356  64601:174")     #cogent
# COMM+=("20473:6601 64600:2914 64600:1299 64600:3356  64602:174")     #cogent
# COMM+=("20473:6601 64600:2914 64600:1299 64600:3356  64603:174")     #cogent
# COMM+=("20473:6601 64600:2914 64600:1299 64600:3356  64600:174")     #cogent
COMM+=("20473:6000") #no-export
# communities
declare -a COMM
#COMM+=("64600:6601")    #no-ix
#COMM+=("64600:2914")    #ntt
#COMM+=("64600:1299")    #telia
#COMM+=("64600:3527")    #gtt
#COMM+=("64600:3356")    #level3
#COMM+=("64600:7922")    #comcast
#COMM+=("64600:174")     #cogent
#COMM+=("64600:6601 64600:2914 64600:1299 64600:3527 64600:3356 64600:7922 64600:174 64600:3491") #pccw
#COMM+=("64600:6000") #no-export

# anycast testbed nodes

declare -a NODES_VULTR
#NODES_VULTR+=("au-syd-anycast01")
NODES_VULTR+=("uk-lnd-anycast02")
#NODES_VULTR+=("fr-par-anycast01")

# start Running communities on VULTR
for community in "${COMM[@]}"
do
#        echo "star for ####################################################################################"
        NODE="uk-lnd-anycast02"
        echo "-------"
        echo "add community $community on node $NODE"  | tee -a $LOG
        $TANGLER_CLI  -4 -t $NODE -c "announce route 145.100.118.0/23 next-hop self community [$community]"
        $TANGLER_CLI -a --csv | tee -a $LOG
#        echo "I'll sleep for" $SLEEP "seconds to wait for the BGP converge"
        sleep $SLEEP  
        ## run VERFPLOETER 
        ACTIVE_BGP_NODES=$($TANGLER_CLI --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")
#    echo "comunitade" $community
        strcomm=$(echo $community | sed "s/ /_/g")
#        echo "strconm" $strcomm
#        echo "ja foi"
        OUTFILE="$REPO/$NODE-VULTR-$strcomm-$NODE-$ACTIVE_BGP_NODES-$DATE_VAR.csv"
#        echo "outrfile"  $OUTFILE
        BGP="Community-$strcomm"x"-$NODE-$ACTIVE_BGP_NODES"
#        echo "BGP" $BGP
        run_verfloeter $OUTFILE $PINGADOR
#        echo "end for ####################################################################################"
done

echo
echo "anycast communities experiment finished: `date` " | tee -a $LOG
echo "-------"
echo
echo
echo "-----------------------------------------------------------"
echo "anycast service is ready for AMPATH communities experiment: `date` " | tee -a $LOG
echo "-------"

# start AMPATH communities
declare -a COMMA
#COMMA+=("20080:700")         # no-ixp
#COMMA+=("20080:700 20080:701") # no-client
#COMMA+=("20080:700 20080:701 20080:702") #no upstream
COMMA+=("20080:801") # prepend-1
COMMA+=("20080:802") # prepend-2
COMMA+=("20080:803") # prepend-3
COMMA+=("20080:804") # prepend-4
COMMA+=("20080:110") # prepend-4
COMMA+=("20080:130") # prepend-4
COMMA+=("20080:150") # prepend-4

for community in "${COMMA[@]}"
do
        NODE="us-mia-anycast01"
        echo "-------"
        echo "add community $community on node $NODE"  | tee -a $LOG
        $TANGLER_CLI  -4 -t $NODE -c "announce route 145.100.118.0/23 next-hop self community [$community]"
        $TANGLER_CLI -a --csv | tee -a $LOG
        echo "I'll sleep for" $SLEEP "seconds to wait for the BGP converge"
        sleep $SLEEP

        ## run VERFPLOETER 
        ACTIVE_BGP_NODES=$($TANGLER_CLI --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g")
        strcomm=$(echo $community | sed "s/ /_/g")
        echo $strcomm
        OUTFILE="$REPO/$NODE-AMPATH-$strcomm-$NODE-$ACTIVE_BGP_NODES-$DATE_VAR.csv"
        BGP="Community-$strcomm"x"-$NODE-$ACTIVE_BGP_NODES"
        run_verfloeter $OUTFILE $PINGADOR

        # remove the prepend
        #echo "remove community from the node $NODE"
        #remove_route_from_node $NODE 
        #sleep $SLEEP

        #add back regular prefix announcement for node
        #add_routes_to_node $NODE
done


    
