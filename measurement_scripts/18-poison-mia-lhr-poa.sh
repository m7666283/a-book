#!/bin/bash
###############################################################################
#  run baseline for anycast testbed
#  Fri Jan 31 18:29:26 CET 2020
#  @copyright ant.isi.edu/paaddos - Leandro Bertholdo - l.m.bertholdo@utwente.nl
#  @copyright sand-project.nl - Joao Ceron - ceron@botlog.org
###############################################################################

# INCLUDES
shopt -s expand_aliases
source 00-globalvar.sh
source 00-functions.sh

###############################################################################
### Program settings
VP_BIN="$HOME/.cargo/bin/verfploeter"
ANYCAST_ADDRESS="145.100.118.1"
ANYCAST_PREFIX="145.100.118.0/23"
EXECDIR=`pwd`
ASN_DB="$EXECDIR/tools/GeoLite2-ASN.mmdb"
GEO_DB="$EXECDIR/tools/GeoLite2-Country.mmdb"
TANGLER_CLI="$EXECDIR/tools/tangler-cli.py -4"
HITLIST="$EXECDIR/tools/hitlist.txt"
VPCLI="$EXECDIR/tools/vp-cli.py"
IP2LOCATION="/usr/local/share/ip2location.bin"
#DATE_VAR=`date -u +\%Y-\%m-\%d-\%T`
#DATE_VAR=`date -u +%Y-%m-%d-%Hh%Mm`
#REPO="$EXECDIR/dataset/`date +%Y-%m-%d-%s`"
#REPO="/Volumes/4T/dataset/`date +%Y-%m-%d-%s`"
BGP="non"
PINGER="us-mia-anycast01"
LOG="$REPO/log.txt"
SLEEP=300

# timestamp
alias utc='date -u +%s'

# REDefine nodes (from 00-globalvar)
unset NODES
declare -a NODES
NODES+=("br-poa-anycast02")
NODES+=("uk-lnd-anycast02")
NODES+=("us-mia-anycast01")


###############################################################################
### Functions
# ----------------------------------------------------------------------------
#tcmd adv br-gru-anycast01 145.100.118.0/24
#tcmd ixp de-fra-anycast01 145.100.118.0/24


poison_vp_outfile_name(){
	local aspath=$1
	echo "------- Verfploeter setup -------" 		| tee -a $LOG
	BGP_POLICY="POISON"
	DRAIN="nodrain"
	
	#ACTIVE_BGP_NODES=$($TANGLER_CLI --nodes-with-announces |  sed "s/-anycast[0-9][0-9]//g" | sed "s/..-//g") 
	ACTIVE_BGP_NODES=$(echo ${NODES[@]} |  sed "s/-anycast[0-9][0-9]//g" | sed "s/..-//g" | sed "s/ /-/g") 

	OUTFILE="$REPO/$BGP_POLICY"
	OUTFILE+="_"$aspath
	OUTFILE+="_PINGER-"$( echo $PINGER | sed "s/-anycast[0-9][0-9]//g" )
	OUTFILE+="_DRAIN-"$( echo $DRAIN | sed "s/-anycast[0-9][0-9]//g" )
	OUTFILE+="_"$ACTIVE_BGP_NODES	
	OUTFILE+="_"`date $DATE_PARAM`".csv" 
}

run_poison(){
	local aspoison="1149_"$1"_1149"
	echo "------- poison [ $aspoison ] -----"
	#tcmd rm
	tcmd poison us-mia-anycast01 $aspoison 145.100.118.0/24
	tcmd
	sleep $SLEEP
	# Assembly verfploeter output file name
	poison_vp_outfile_name "$aspoison"
	echo "outfile [$OUTFILE]"                       | tee -a $LOG
	# Run verfploeter
	#run_verfploeter_looped "$OUTFILE" "$PINGER" 
	run_verfloeter  "$OUTFILE" "$PINGER" 

}


###############################################################################
### MAIN
###############################################################################

# create repository dir
[ ! -d $REPO ] && mkdir $REPO

# update symlink
dir_=`dirname $REPO`
ln -sfn $REPO $dir_/last

echo "logfile in: $LOG"
## initial setup
date | tee -a $LOG

# check the VPN Tunnel
check_tunnel

# show defined nodes
show_nodes | tee -a $LOG

# show verfploeter nodes connected
$VP_BIN cli client-list | tee -a $LOG


############################################################
## Anycast distribution - regular monitoring - BASELINE
############################################################

# CLEANING ALL
# this will withdraw all the prefix over all the nodes
echo "removing all routes on all known nodes"
#$TANGLER_CLI -4 -w -v
$TANGLER_CLI -4 -w 

echo "-------"
echo " anycast baseline started: `date` " | tee -a $LOG
echo "-------"

# Advertise on all nodes
#tcmd adv 145.100.118.0/24

# Advertise on selected nodes with prepends (-3xMIA)
#tcmd prepend br-poa-anycast02 1149_1149_1149 145.100.118.0/24
#tcmd prepend uk-lnd-anycast02 1149_1149_1149 145.100.118.0/24
#tcmd add us-mia-anycast01 145.100.118.0/24

# Advertise normal baseline
tcmd add br-poa-anycast02 145.100.118.0/24
tcmd add uk-lnd-anycast02 145.100.118.0/24
tcmd add us-mia-anycast01 145.100.118.0/24

# Settle down
sleep $SLEEP
poison_vp_outfile_name "baseline"
run_verfploeter "$OUTFILE" "$PINGER" 


# prepend one in MIA
tcmd prepend us-mia-anycast01 1149_1149 145.100.118.0/24
poison_vp_outfile_name "1149_1149"
sleep $SLEEP
run_verfploeter "$OUTFILE" "$PINGER"

# prepend two in MIA
tcmd prepend us-mia-anycast01 1149_1149_1149 145.100.118.0/24
poison_vp_outfile_name "1149_1149_1149"
sleep $SLEEP
run_verfploeter "$OUTFILE" "$PINGER"


#
# Do poison on MIA + run verfploeter
# 
run_poison 63221  #FL-IX  
run_poison 24115  #EQ-MIA

run_poison 2914
run_poison 6939

run_poison 7018
run_poison 701
run_poison 6762
run_poison 3257

run_poison 6461
run_poison 12953
run_poison 174
run_poison 1239
run_poison 1273
run_poison 1299
run_poison 3320
run_poison 3356
run_poison 5511
run_poison 6453

run_poison 63221_24115

run_poison  1916
run_poison  1251
run_poison  16735  # Algar
run_poison  262589 # Internexa


echo "-------"
echo "finished poison experiment"
echo "-------"
echo
    
