Tue Feb 22 14:15:27 UTC 2022
Checking VPN Tunnel
Show defined nodes
we are using 3 nodes in this experiment
br-poa-anycast02
us-mia-anycast01
nl-ams-anycast01
show verfploeter cli client-list
+-------+------------------+---------+
| Index | Hostname         | Version |
| 5     | br-poa-anycast02 | 0.1.42  |
| 11    | us-mia-anycast01 | 0.1.42  |
| 21    | uk-lnd-anycast02 | 0.1.42  |
| 12    | us-was-anycast01 | 0.1.42  |
| 23    | za-jnb-anycast01 | 0.1.42  |
| 25    | nl-ens-anycast02 | 0.1.42  |
| 2     | us-los-anycast01 | 0.1.42  |
| 3     | dk-cop-anycast01 | 0.1.42  |
| 8     | de-fra-anycast01 | 0.1.42  |
| 15    | us-sea-anycast01 | 0.1.42  |
| 20    | au-syd-anycast01 | 0.1.42  |
| 6     | br-gru-anycast01 | 0.1.42  |
| 14    | nl-ams-anycast01 | 0.1.42  |
| 13    | fr-par-anycast01 | 0.1.42  |
| 22    | sg-sin-anycast01 | 0.1.42  |
| 1     | nl-arn-anycast01 | 0.1.42  |
+-------+------------------+---------+
Connected clients: 16
withdraw_all_running_nodes
 anycast baseline started: Tue Feb 22 14:15:56 UTC 2022 
 We are generating packet in: us-was-anycast01 
 We set this BGP Policy: baseline-#ipv4,us-mia,nl-ams,br-poa 
/home/testbed/.cargo/bin/verfploeter cli start us-was-anycast01 145.100.118.1  /home/testbed/work/catchment_manipulation/tools/hitlist.txt  -a /home/testbed/work/catchment_manipulation/tools/GeoLite2-ASN.mmdb -c /home/testbed/work/catchment_manipulation/tools/GeoLite2-Country.mmdb  > /home/testbed/work/catchment_manipulation/dataset/2022-02-22-14h15m/baseline-#ipv4,us-mia,nl-ams,br-poa.csv
