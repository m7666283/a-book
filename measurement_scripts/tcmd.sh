#!/bin/bash
###############################################################################
#  run baseline for anycast testbed
#  Fri Jan 31 18:29:26 CET 2020
#  @copyright ant.isi.edu/paaddos - Leandro Bertholdo - l.m.bertholdo@utwente.nl
#  @copyright sand-project.nl - Joao Ceron - ceron@botlog.org
###############################################################################

# include functions and aliases
source 00-globalvar.sh
source 00-functions.sh
shopt -s expand_aliases


###############################################################################
### Program settings

LOG="/tmp/tcmdlog.txt"

# timestamp
alias utc='date -u +%s'




###############################################################################
### MAIN
###############################################################################

tcmd $1 $2 $3 $4 $5 

#show_all_nodes




