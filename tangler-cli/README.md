# Tangled: Routing and measurement scripts with EXABGP and VERFPLOTER 
### TestBed: tangled-cli.py and vp-cli.py

## DESCRIPTION:

### TANGLER-CLI.py

tangled-cli tool is used to control BGP routing in an anycast networks. This tool uses EXABGP software to inject routes on each anycast site. This tool is used on Tangled Testbed (https://anycast-testbed.nl/).

The BGP routing announces from each anycast site is controlled in a central server (Tangled master).
This central server use TANGLER-CLI.py to BGP dynamic routing configuration.

## REQUIREMENTS:
To run this software it is necessary first to setup an anycast network and BGP sessions with ISPs and IXPs. Anycast sites need to be configured on tangler-cli.py (hardcoded).
Is necessary to grant SSH access to from the master server to all anycast sites. TANGLER-CLI.py and VP-CLI.py run on master anycast site.

## TANGLED-cli Options:
```
usage: ./tangler-cli [options]

optional arguments:
  -h, --help            show this help message and exit
  --version             print version and exit
  -d, --debug           print debug messages
  -v                    print verbose messages
  -4                    working on IPv4 neighbor
  -6                    working on IPv6 neighbor
  --status              status of neighbor
  --csv                 CSV output
  -a, --announces       active announces
  -t [TARGET], --target [TARGET]
                        target [node|all]
  -c [CMD], --cmd [CMD]
                        cmd to be executed
  -A                    announce routes [-r] to a target [-t] or all nodes/routes (default)
  -P [PREPEND]          number of prepends
  -r [ROUTE]            BGP route to be added
  --key [KEY]           SSH key present on the testbed
  --user [USER]         SSH user used to login on the testbed
  -w                    withdraw routes [-r] from a target [-t] or all nodes/routes (default)
  --nodes-with-announces
                        list the name of nodes with active prefix announcement
```

### Example of use:
```
./tangler-cli.py -4  -t us-mia-anycast01 -A -r 145.100.118.0/23
```


### Software versions

The last version of both this software can be found on [https://github.com/LMBertholdo/TANGLED-cli](https://github.com/LMBertholdo/TANGLED-cli)
