# Tangled: Routing and measurement scripts with EXABGP and VERFPLOTER 
### TestBed: tangled-cli.py and vp-cli.py

## DESCRIPTION:

VP-CLI.py is used to process Verfploeter raw files to generate the statistics used to build the anycast playbook.


## REQUIREMENTS:
To run this software you need a measurement file obtained by running Verfpleoter (1.0.42) in CSV format.

## VP-cli Options:

```
➜  TANGLED-cli :(master) ✗ ./vp-cli.py --help
usage: vp-cli.py [options]

optional arguments:
  -h, --help            show this help message and exit
  --version             print version and exit
  -v, --verbose         print info msg
  -d, --debug           print debug info
  -q, --quiet           ignore animation
  -f [FILE], --file [FILE]
                        Verfploeter measurement output file
  -n, --normalize       remove inconsistency from the measurement dataset and rebuild geolocation
  -g GEO [GEO ...], --geo GEO [GEO ...]
                        geo-location database - IP2Location (BIN)
  --hitlist HITLIST [HITLIST ...]
                        IPv4 hitlist - used to find unknown stats
  -s [SOURCE], --source [SOURCE]
                        Verfploeter source pinger node to be inserted as metadata
  -b [BGP], --bgp [BGP]
                        BGP policy to be inserted as metadata
  -w WEIGHT [WEIGHT ...], --weight WEIGHT [WEIGHT ...]
                        File used to weight the /24. Use the SIDN load file.
  --filter              Build a new file with the intersection of `weighted` and input file.
  --csv                 print server load distribution using csv
```



### Example of use:
```
./vp-cli.py -q -f anycast-prepend-2x-fr-par-anycast01-2019-10-03-05:28:57.csv.gz > load-prepend-2x-fr-par.txt
```


### Software versions

The last version of both this software can be found on [https://github.com/LMBertholdo/TANGLED-cli](https://github.com/LMBertholdo/TANGLED-cli)
